# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.290');

# ---------------------------------------------------------------------- #
# Andris Stock Changes     												 #
# ---------------------------------------------------------------------- # 
CREATE TABLE sp_grn (
						SpGRNID INT(10) NULL AUTO_INCREMENT, 
						PrintedDate TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, 
						PrintedUserID INT NULL, SPPartOrderID INT NULL, 
						ServiceProviderID INT NULL, PRIMARY KEY (SpGRNID) 
					) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

ALTER TABLE sp_part_stock_item ADD COLUMN SpGRNID INT(11) NULL DEFAULT NULL AFTER SpStockReceivingHistoryID;

ALTER TABLE part ADD COLUMN SpGRNID INT(11) NULL DEFAULT NULL AFTER ChargeType;



# ---------------------------------------------------------------------- #
# Nag Job Settings Changes     											 #
# ---------------------------------------------------------------------- # 
RENAME TABLE job_settings TO job_settings_old ;

CREATE TABLE IF NOT EXISTS job_settings (
											JobSettingsID int(11) NOT NULL AUTO_INCREMENT, 
											NetworkID int(11) DEFAULT NULL,
											ClientID int(11) DEFAULT NULL, 
											JobTypeID int(11) NOT NULL, 
											FieldName varchar(64) NOT NULL, 
											FieldValue varchar(255) DEFAULT NULL, 
											Display enum('0','1') DEFAULT '0', 
											Mandatory enum('0','1') DEFAULT '0', 
											Replicate enum('0','1') DEFAULT '0', 
											Status enum('Active','In-Active') DEFAULT 'Active', 
											CreatedDate timestamp NULL DEFAULT '0000-00-00 00:00:00', 
											EndDate timestamp NULL DEFAULT '0000-00-00 00:00:00', 
											ModifiedUserID int(11) DEFAULT NULL, 
											ModifiedDate timestamp NULL DEFAULT CURRENT_TIMESTAMP, 
											PRIMARY KEY (JobSettingsID) 
										) ENGINE=InnoDB AUTO_INCREMENT=1 ;

ALTER TABLE job_settings ADD INDEX ( NetworkID , ClientID , JobTypeID ) ;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.291');
