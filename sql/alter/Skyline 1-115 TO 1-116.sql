# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-08 09:55                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.115');

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                               #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider_engineer` ADD COLUMN `MondayActive` ENUM('yes','no') DEFAULT 'no';

ALTER TABLE `service_provider_engineer` ADD COLUMN `MondayStartShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `MondayEndShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `MondayStartPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `MondayEndPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `TuesdayActive` ENUM('yes','no') DEFAULT 'no';

ALTER TABLE `service_provider_engineer` ADD COLUMN `TuesdayStartShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `TuesdayEndShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `TuesdayStartPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `TuesdayEndPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `WednesdayActive` ENUM('yes','no') DEFAULT 'no';

ALTER TABLE `service_provider_engineer` ADD COLUMN `WednesdayStartShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `WednesdayEndShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `WednesdayStartPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `WednesdayEndPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `ThursdayActive` ENUM('yes','no') DEFAULT 'no';

ALTER TABLE `service_provider_engineer` ADD COLUMN `ThursdayStartShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `ThursdayEndShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `ThursdayStartPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `ThursdayEndPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `FridayActive` ENUM('yes','no') DEFAULT 'no';

ALTER TABLE `service_provider_engineer` ADD COLUMN `FridayStartShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `FridayEndShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `FridayStartPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `FridayEndPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `SaturdayActive` ENUM('yes','no') DEFAULT 'no';

ALTER TABLE `service_provider_engineer` ADD COLUMN `SaturdayStartShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `SaturdayEndShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `SaturdayStartPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `SaturdayEndPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `SundayActive` ENUM('yes','no') DEFAULT 'no';

ALTER TABLE `service_provider_engineer` ADD COLUMN `SundayStartShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `SundayEndShift` TIME;

ALTER TABLE `service_provider_engineer` ADD COLUMN `SundayStartPostcode` VARCHAR(8);

ALTER TABLE `service_provider_engineer` ADD COLUMN `SundayEndPostcode` VARCHAR(8);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.116');
