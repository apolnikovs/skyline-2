# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.180');

# ---------------------------------------------------------------------- #
# Modify table "job"                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE `job` CHANGE COLUMN `UnitCondition` `UnitCondition` VARCHAR(150) NULL DEFAULT NULL AFTER `Accessories`;


 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.181');
