# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.160');


# ---------------------------------------------------------------------- #
# Modify table "service_type_alias"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_type_alias` ADD COLUMN `ServiceTypeMask` VARCHAR(40) NULL DEFAULT NULL AFTER `ServiceTypeID`;




# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.161');




