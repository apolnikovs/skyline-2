# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-09-20 14:35                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.81');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `permission` DROP FOREIGN KEY `user_TO_permission`;

ALTER TABLE `job` DROP FOREIGN KEY `network_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `branch_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `client_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `customer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `product_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `manufacturer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `job_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `status_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `model_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job_ModifiedUser`;

ALTER TABLE `job` DROP FOREIGN KEY `county_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `country_TO_job`;

ALTER TABLE `appointment` DROP FOREIGN KEY `job_TO_appointment`;

ALTER TABLE `appointment` DROP FOREIGN KEY `user_TO_appointment`;

ALTER TABLE `assigned_permission` DROP FOREIGN KEY `permission_TO_assigned_permission`;

ALTER TABLE `audit` DROP FOREIGN KEY `job_TO_audit`;

ALTER TABLE `part` DROP FOREIGN KEY `job_TO_part`;

ALTER TABLE `contact_history` DROP FOREIGN KEY `job_TO_contact_history`;

ALTER TABLE `status_history` DROP FOREIGN KEY `job_TO_status_history`;

ALTER TABLE `claim_response` DROP FOREIGN KEY `job_TO_claim_response`;

# ---------------------------------------------------------------------- #
# Drop table "appointments"                                              #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `appointments` MODIFY `AppointmentID` INTEGER NOT NULL;

# Drop constraints #

ALTER TABLE `appointments` ALTER COLUMN `NonSkylineJob` DROP DEFAULT;

ALTER TABLE `appointments` ALTER COLUMN `OutCardLeft` DROP DEFAULT;

ALTER TABLE `appointments` ALTER COLUMN `AppointmentOrphaned` DROP DEFAULT;

ALTER TABLE `appointments` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `appointments`;

# ---------------------------------------------------------------------- #
# Drop table "AppointmentSource"                                         #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `AppointmentSource` MODIFY `AppointmentSourceID` INTEGER NOT NULL;

# Drop constraints #

ALTER TABLE `AppointmentSource` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `AppointmentSource`;

# ---------------------------------------------------------------------- #
# Drop table "engineer_skillset"                                         #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `engineer_skillset` MODIFY `EngineerSkillsetID` INTEGER NOT NULL;

# Drop constraints #

ALTER TABLE `engineer_skillset` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `engineer_skillset`;

# ---------------------------------------------------------------------- #
# Drop table "JobSource"                                                 #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `JobSource` MODIFY `JobSourceID` INTEGER NOT NULL;

# Drop constraints #

ALTER TABLE `JobSource` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `JobSource`;

# ---------------------------------------------------------------------- #
# Modify table "permission"                                              #
# ---------------------------------------------------------------------- #

ALTER TABLE `permission` ADD COLUMN `URLSegment` VARCHAR(50);

# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` ADD COLUMN `JobSourceID` INTEGER;

CREATE INDEX `IDX_job_1` ON `job` (`GuaranteeCode`);

CREATE INDEX `IDX_job_2` ON `job` (`DateBooked`);

CREATE INDEX `IDX_job_3` ON `job` (`ClosedDate`);

CREATE INDEX `IDX_job_19` ON `job` (`JobSourceID`);

# ---------------------------------------------------------------------- #
# Modify table "appointment"                                             #
# ---------------------------------------------------------------------- #

ALTER TABLE `appointment` ADD COLUMN `ServiceProviderID` INTEGER;

ALTER TABLE `appointment` ADD COLUMN `NonSkylineJobID` INTEGER;

ALTER TABLE `appointment` ADD COLUMN `DiaryAllocationID` INTEGER;

ALTER TABLE `appointment` ADD COLUMN `AppointmentStartTime` TIME;

ALTER TABLE `appointment` ADD COLUMN `AppointmentEndTime` TIME;

ALTER TABLE `appointment` ADD COLUMN `SeviceProviderEngineerID` INTEGER;

ALTER TABLE `appointment` ADD COLUMN `CreatedUserID` INTEGER;

ALTER TABLE `appointment` ADD COLUMN `AppointmentOrphaned` ENUM('Yes','No');

ALTER TABLE `appointment` ADD COLUMN `AppointmentStatusID` INTEGER;

CREATE INDEX `IDX_appointment_1` ON `appointment` (`AppointmentDate`);

CREATE INDEX `IDX_appointment_4` ON `appointment` (`ServiceProviderID`);

CREATE INDEX `IDX_appointment_5` ON `appointment` (`NonSkylineJobID`);

CREATE INDEX `IDX_appointment_6` ON `appointment` (`DiaryAllocationID`);

CREATE INDEX `IDX_appointment_7` ON `appointment` (`SeviceProviderEngineerID`);

CREATE INDEX `IDX_appointment_8` ON `appointment` (`ServiceProviderID`);

CREATE INDEX `IDX_appointment_9` ON `appointment` (`NonSkylineJobID`);

CREATE INDEX `IDX_appointment_10` ON `appointment` (`DiaryAllocationID`);

CREATE INDEX `IDX_appointment_11` ON `appointment` (`AppointmentDate`);

CREATE INDEX `IDX_appointment_12` ON `appointment` (`SeviceProviderEngineerID`);

# ---------------------------------------------------------------------- #
# Modify table "Welcome_Message_Defaults"                                #
# ---------------------------------------------------------------------- #

ALTER TABLE `Welcome_Message_Defaults` MODIFY `WelcomeMessageDefaultsID` INTEGER NOT NULL;

ALTER TABLE `Welcome_Message_Defaults` ADD
    PRIMARY KEY (`WelcomeMessageDefaultsID`);

# ---------------------------------------------------------------------- #
# Modify table "Welcome_Messages"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE `Welcome_Messages` MODIFY `WelcomeMessageID` INTEGER NOT NULL;

ALTER TABLE `Welcome_Messages` ADD
    PRIMARY KEY (`WelcomeMessageID`);

# ---------------------------------------------------------------------- #
# Modify table "diary_allocation"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE `diary_allocation` DROP COLUMN `DiaryPostcodeAllocationID`;

ALTER TABLE `diary_allocation` CHANGE `ServiceProviderEngineerSkillsetID` `ServiceProviderSkillsetID` INTEGER;

CREATE INDEX `IDX_diary_allocation_1` ON `diary_allocation` (`ServiceProviderID`);

CREATE INDEX `IDX_diary_allocation_2` ON `diary_allocation` (`AllocatedDate`);

CREATE INDEX `IDX_diary_allocation_3` ON `diary_allocation` (`AppointmentAllocationSlotID`);

# ---------------------------------------------------------------------- #
# Modify table "diary_town_allocation"                                   #
# ---------------------------------------------------------------------- #

CREATE INDEX `IDX_diary_town_allocation_1` ON `diary_town_allocation` (`DiaryAllocationID`);

# ---------------------------------------------------------------------- #
# Modify table "diary_postcode_allocation"                               #
# ---------------------------------------------------------------------- #

CREATE INDEX `IDX_diary_postcode_allocation_1` ON `diary_postcode_allocation` (`DiaryAllocationID`);

# ---------------------------------------------------------------------- #
# Add table "appointment_source"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `appointment_source` (
    `AppointmentSourceID` INTEGER NOT NULL AUTO_INCREMENT,
    `Description` VARCHAR(50),
    CONSTRAINT `appointment_sourceID` PRIMARY KEY (`AppointmentSourceID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                               #
# ---------------------------------------------------------------------- #

CREATE INDEX `IDX_service_provider_engineer_1` ON `service_provider_engineer` (`ServiceProviderID`);

# ---------------------------------------------------------------------- #
# Add table "service_provider_skillset"                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_skillset` (
    `ServiceProviderSkillsetID` INTEGER NOT NULL AUTO_INCREMENT,
    `SkillsetID` INTEGER,
    `ServiceDuration` SMALLINT(4),
    `ServiceProviderID` INTEGER,
    `EngineersRequired` TINYINT(2) DEFAULT 1,
    CONSTRAINT `service_provider_skillsetID` PRIMARY KEY (`ServiceProviderSkillsetID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_service_provider_skillset_1` ON `service_provider_skillset` (`ServiceProviderID`);

CREATE INDEX `IDX_service_provider_skillset_2` ON `service_provider_skillset` (`SkillsetID`);

# ---------------------------------------------------------------------- #
# Add table "skillset"                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `skillset` (
    `SkillsetID` INTEGER NOT NULL AUTO_INCREMENT,
    `SkillsetName` VARCHAR(40),
    `ServiceDuration` SMALLINT(4),
    `EngineersRequired` TINYINT(2) DEFAULT 1,
    `RepairSkillID` INTEGER,
    `JobTypeID` INTEGER,
    `ServiceTypeID` INTEGER,
    CONSTRAINT `skillsetID` PRIMARY KEY (`SkillsetID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Modify table "non_skyline_job"                                         #
# ---------------------------------------------------------------------- #

ALTER TABLE `non_skyline_job` ADD COLUMN `RepairSkillID` INTEGER;

ALTER TABLE `non_skyline_job` ADD COLUMN `JobTypeID` INTEGER;

ALTER TABLE `non_skyline_job` ADD COLUMN `ServiceTypeID` INTEGER;

CREATE INDEX `IDX_non_skyline_job_1` ON `non_skyline_job` (`ServiceProviderID`);

CREATE INDEX `IDX_non_skyline_job_2` ON `non_skyline_job` (`AppointmentID`);

CREATE INDEX `IDX_non_skyline_job_3` ON `non_skyline_job` (`ServiceProviderJobNo`);

# ---------------------------------------------------------------------- #
# Add table "job_source"                                                 #
# ---------------------------------------------------------------------- #

CREATE TABLE `job_source` (
    `JobSourceID` INTEGER NOT NULL AUTO_INCREMENT,
    `Description` VARCHAR(50),
    CONSTRAINT `job_sourceID` PRIMARY KEY (`JobSourceID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "email"                                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `email` (
    `EmailID` INTEGER NOT NULL AUTO_INCREMENT,
    `Title` VARCHAR(100),
    `Message` TEXT,
    `ModifiedDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `ModifiedUserID` INTEGER,
    CONSTRAINT `emailID` PRIMARY KEY (`EmailID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_email_ModifiedUserID_FK` ON `email` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "email_job"                                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `email_job` (
    `EmailJobID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER,
    `EmailID` INTEGER,
    `ModifiedDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `ModifiedUserID` INTEGER,
    CONSTRAINT `email_jobID` PRIMARY KEY (`EmailJobID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_email_job_JobID_FK` ON `email_job` (`JobID`);

CREATE INDEX `IDX_email_job_EmailID_FK` ON `email_job` (`EmailID`);

CREATE INDEX `IDX_email_job_ModifiedUserID_FK` ON `email_job` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer_skillset"                      #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `service_provider_engineer_skillset` MODIFY `ServiceProviderEngineerSkillsetID` INTEGER NOT NULL;

ALTER TABLE `service_provider_engineer_skillset` DROP PRIMARY KEY;

ALTER TABLE `service_provider_engineer_skillset` DROP COLUMN `EngineerSkillsetID`;

ALTER TABLE `service_provider_engineer_skillset` DROP COLUMN `ServiceDuration`;

ALTER TABLE `service_provider_engineer_skillset` ADD COLUMN `ServiceProviderSkillsetID` INTEGER;

ALTER TABLE `service_provider_engineer_skillset` ADD COLUMN `ServiceProviderEngineerID` INTEGER;

ALTER TABLE `service_provider_engineer_skillset` CHANGE `ServiceProviderEngineerSkillsetID` `SeviceProviderEngineerSkillsetID` INTEGER NOT NULL;

ALTER TABLE `service_provider_engineer_skillset` MODIFY `SeviceProviderEngineerSkillsetID` INTEGER NOT NULL;

ALTER TABLE `service_provider_engineer_skillset` ADD CONSTRAINT `service_provider_engineer_skillsetID` 
    PRIMARY KEY (`SeviceProviderEngineerSkillsetID`);

CREATE INDEX `IDX_service_provider_engineer_skillset_1` ON `service_provider_engineer_skillset` (`ServiceProviderSkillsetID`);

CREATE INDEX `IDX_service_provider_engineer_skillset_2` ON `service_provider_engineer_skillset` (`ServiceProviderEngineerID`);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `permission` ADD CONSTRAINT `user_TO_permission` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `network_TO_job` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `job` ADD CONSTRAINT `branch_TO_job` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `job` ADD CONSTRAINT `product_TO_job` 
    FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`);

ALTER TABLE `job` ADD CONSTRAINT `service_type_TO_job` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `manufacturer_TO_job` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `job` ADD CONSTRAINT `job_type_TO_job` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `status_TO_job` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `job` ADD CONSTRAINT `model_TO_job` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job` 
    FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job_ModifiedUser` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `county_TO_job` 
    FOREIGN KEY (`ColAddCountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `job` ADD CONSTRAINT `country_TO_job` 
    FOREIGN KEY (`ColAddCountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `appointment` ADD CONSTRAINT `job_TO_appointment` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `user_TO_appointment` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `email` ADD CONSTRAINT `user_TO_email` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `email_job` ADD CONSTRAINT `job_TO_email_job` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `email_job` ADD CONSTRAINT `email_TO_email_job` 
    FOREIGN KEY (`EmailID`) REFERENCES `email` (`EmailID`);

ALTER TABLE `email_job` ADD CONSTRAINT `user_TO_email_job` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `assigned_permission` ADD CONSTRAINT `permission_TO_assigned_permission` 
    FOREIGN KEY (`PermissionID`) REFERENCES `permission` (`PermissionID`);

ALTER TABLE `audit` ADD CONSTRAINT `job_TO_audit` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `part` ADD CONSTRAINT `job_TO_part` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `contact_history` ADD CONSTRAINT `job_TO_contact_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `status_history` ADD CONSTRAINT `job_TO_status_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `claim_response` ADD CONSTRAINT `job_TO_claim_response` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.82');
