
# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.236');


# ---------------------------------------------------------------------- #
# Modify table quality_assurance_questions                               #
# ---------------------------------------------------------------------- #
CREATE TABLE `quality_assurance_questions` (
	`QAQuestionID` INT(11) NOT NULL AUTO_INCREMENT,
	`BrandID` INT(11) NOT NULL,
	`QuestionText` VARCHAR(250) NOT NULL,
	`QuestionComment` TEXT NULL,
	`QuestionPosition` INT(11) NULL DEFAULT NULL,
	`ModifiedUserID` INT(11) NULL DEFAULT NULL,
	`ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`QAQuestionID`),
	INDEX `brand_TO_quality_assurance_questions` (`BrandID`),
	INDEX `user_TO_quality_assurance_questions` (`ModifiedUserID`),
	CONSTRAINT `brand_TO_quality_assurance_questions` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
	CONSTRAINT `user_TO_quality_assurance_questions` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=9;


# ---------------------------------------------------------------------- #
# Modify table quality_assurance_data                                    #
# ---------------------------------------------------------------------- #
CREATE TABLE `quality_assurance_data` (
	`QADataID` INT(11) NOT NULL AUTO_INCREMENT,
	`QAQuestionID` INT(11) NOT NULL,
	`JobID` INT(11) NOT NULL,
	`QAData` TINYINT(1) NOT NULL,
	`ModifiedUserID` INT(11) NULL DEFAULT NULL,
	`ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`QADataID`),
	INDEX `quality_assurance_questions_TO_quality_assurance_data` (`QAQuestionID`),
	INDEX `job_TO_quality_assurance_data` (`JobID`),
	INDEX `user_TO_quality_assurance_data` (`ModifiedUserID`),
	CONSTRAINT `job_TO_quality_assurance_data` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
	CONSTRAINT `quality_assurance_questions_TO_quality_assurance_data` FOREIGN KEY (`QAQuestionID`) REFERENCES `quality_assurance_questions` (`QAQuestionID`),
	CONSTRAINT `user_TO_quality_assurance_data` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=2;


 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.237');
