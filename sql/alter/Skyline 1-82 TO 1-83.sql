# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-09-24 10:40                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.82');

# ---------------------------------------------------------------------- #
# Modify table "service_provider_skillset"                               #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider_skillset` ADD COLUMN `Status` ENUM('Active','In-active') DEFAULT 'Active';

# ---------------------------------------------------------------------- #
# Modify table "skillset"                                                #
# ---------------------------------------------------------------------- #

ALTER TABLE `skillset` ADD COLUMN `Status` ENUM('Active','In-active') DEFAULT 'Active';

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer_skillset"                      #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider_engineer_skillset` DROP PRIMARY KEY;

ALTER TABLE `service_provider_engineer_skillset` CHANGE `SeviceProviderEngineerSkillsetID` `ServiceProviderEngineerSkillsetID` INTEGER NOT NULL;

ALTER TABLE `service_provider_engineer_skillset` ADD CONSTRAINT `service_provider_engineer_skillsetID` 
    PRIMARY KEY (`ServiceProviderEngineerSkillsetID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.83');
