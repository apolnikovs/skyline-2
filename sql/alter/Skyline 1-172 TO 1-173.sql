# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.172');

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                               #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider_engineer` ADD `ReplicatePostcodeAllocation` ENUM( 'Yes', 'No' ) NULL DEFAULT 'Yes';
ALTER TABLE `service_provider_engineer` CHANGE COLUMN `StartHomePostcode` `StartHomePostcode` VARCHAR(100) NULL DEFAULT NULL COMMENT 'Used as main start pc' AFTER `EndLocation`;
ALTER TABLE `service_provider_engineer` CHANGE COLUMN `EndHomePostcode` `EndHomePostcode` VARCHAR(100) NULL DEFAULT NULL COMMENT 'Used as main end pc' AFTER `StartOtherPostcode`;

# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider` ADD `SendRouteMapsEmail` TIME NULL;
ALTER TABLE `service_provider` ADD `Multimaps` INT( 2 ) NULL DEFAULT '6' ;

# ---------------------------------------------------------------------- #
# Modify table "user"                                                    #
# ---------------------------------------------------------------------- #
ALTER TABLE `user` ADD `GaDial0Yellow` INT(11) NOT NULL DEFAULT 60;
ALTER TABLE `user` ADD `GaDial0Red` INT(11) NOT NULL DEFAULT 80;
ALTER TABLE `user` ADD `GaDial1Yellow` INT(11) NOT NULL DEFAULT 60;
ALTER TABLE `user` ADD `GaDial1Red` INT(11) NOT NULL DEFAULT 80;
ALTER TABLE `user` ADD `GaDial2Yellow` INT(11) NOT NULL DEFAULT 60;
ALTER TABLE `user` ADD `GaDial2Red` INT(11) NOT NULL DEFAULT 80;
ALTER TABLE `user` ADD `GaDial3Yellow` INT(11) NOT NULL DEFAULT 600;
ALTER TABLE `user` ADD `GaDial3Red` INT(11) NOT NULL DEFAULT 800;
ALTER TABLE `user` ADD `GaDial3Max` INT(11) NOT NULL DEFAULT 1000;
ALTER TABLE `user` ADD `GaDial4Yellow` INT(11) NOT NULL DEFAULT 600;
ALTER TABLE `user` ADD `GaDial4Red` INT(11) NOT NULL DEFAULT 800;
ALTER TABLE `user` ADD `GaDial4Max` INT(11) NOT NULL DEFAULT 1000;
ALTER TABLE `user` ADD `GaDial5Yellow` INT(11) NOT NULL DEFAULT 600;
ALTER TABLE `user` ADD `GaDial5Red` INT(11) NOT NULL DEFAULT 800;
ALTER TABLE `user` ADD `GaDial5Max` INT(11) NOT NULL DEFAULT 1000;
ALTER TABLE `user` ADD `GaDialBoundaryLower` INT(11) NOT NULL DEFAULT 7;
ALTER TABLE `user` ADD `GaDialBoundaryUpper` INT(11) NOT NULL DEFAULT 7;


# ---------------------------------------------------------------------- #
# Add table "sp_grid_defaults"                                           #
# ---------------------------------------------------------------------- #
CREATE TABLE `sp_grid_defaults` (
`SpGridDefaultsID` INT(10) NOT NULL AUTO_INCREMENT,
`ServiceProviderID` INT(10) NULL,
`Lat1` DECIMAL(10,2) NULL DEFAULT NULL,
`Lng1` DECIMAL(10,2) NULL DEFAULT NULL,
`Lat2` DECIMAL(10,2) NULL DEFAULT NULL,
`Lng2` DECIMAL(10,2) NULL DEFAULT NULL,
`CellSize` DECIMAL(10,2) NOT NULL DEFAULT '1',
`UpdatedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
`UserUpdated` VARCHAR(50) NOT NULL,
PRIMARY KEY (`SpGridDefaultsID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.173');
