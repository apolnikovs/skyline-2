////
// pccs.string.function.js
// 
// This contains generic JavaScript funcrtions reklating strings for SkyLine
//
// @author     Andrew J. Wiliams <a.williams@pccsuk.com>
// @copyright  2013 PC Control Systems
// @link       http://www.pccontrolsystems.com
// @version    1.0
// 
//  
// Changes
// Date        Version Author                Reason
// 11/12/2012  1.00    Andrew J. Williams    Initial Version (toTitleCase)
////////////////////////////////////////////////////////////////////////////////

////
// JobsGauges
// 
// Convert a string to Title Case with option for not changing some words (eg
// link words such as the, of, and words that should be captilised such as ID, TV)
//
// @param string str    The string to be converted
// @return 
// @author http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
//  
/////////////////////////////////////////////////////////////////////////////////////////

var toTitleCase = function(str) {
    var i, str, lowers, uppers;
    str = str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });

    // Certain minor words should be left lowercase unless 
    // they are the first or last words in the string
    lowers = ['A', 'An', 'The', 'And', 'But', 'Or', 'For', 'Nor', 'As', 'At', 
    'By', 'For', 'From', 'In', 'Into', 'Near', 'Of', 'On', 'Onto', 'To', 'With'];
    for (i = 0; i < lowers.length; i++)
        str = str.replace(new RegExp('\\s' + lowers[i] + '\\s', 'g'), 
            function(txt) {
                return txt.toLowerCase();
            });

    // Certain words such as initialisms or acronyms should be left uppercase
    uppers = ['Id', 'Tv', 'Pvr', 'Dvd', 'Dbs', 'Ictv', 'Lcd', 'Av', 'Fx', 'Svs', 'Dsc', 'Mwes', 'Jvc'];
    for (i = 0; i < uppers.length; i++)
        str = str.replace(new RegExp('\\b' + uppers[i] + '\\b', 'g'), 
            uppers[i].toUpperCase());

    return str;
}
