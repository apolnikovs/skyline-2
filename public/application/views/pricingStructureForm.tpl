{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <script type="text/javascript" >
        
       
              
        
    $(document).ready(function() {
       $("#NetworkID").combobox({
            change: function() {
                $CompletionStatusDropDownList = $ManufacturerDropDownList =  $clientDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';           
                var $NetworkID = $("#NetworkID").val();
                if($NetworkID && $NetworkID!='')
                {
                    //Getting clients of network
                    $.post("{$_subdomain}/ProductSetup/pricingStructure/getClients/"+urlencode($NetworkID)+"/", '', function(data){
                        var $networkClients = eval("(" + data + ")");
                        if($networkClients)
                        {
                            for(var $i=0;$i<$networkClients.length;$i++)
                            {
                                $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                            }
                        }
                        $("#ClientID").html($clientDropDownList);
                    });
                     //Getting manufacturers of network
                    $.post("{$_subdomain}/ProductSetup/pricingStructure/getManufacturers/"+urlencode($NetworkID)+"/", '', function(data){
                        var $networkManufacturers = eval("(" + data + ")");
                        if($networkManufacturers)
                        {
                            for(var $i=0;$i<$networkManufacturers.length;$i++)
                            {
                                $ManufacturerDropDownList += '<option value="'+$networkManufacturers[$i]['ManufacturerID']+'" >'+$networkManufacturers[$i]['ManufacturerName']+'</option>';
                            }
                        }
                        $("#ManufacturerID").html($ManufacturerDropDownList);
                    });
                    //Getting completion statuses of network    
                     $.post("{$_subdomain}/ProductSetup/pricingStructure/getCompletionStatuses/"+urlencode($NetworkID)+"/", '', function(data){
                        var $networkCompletionStatuses = eval("(" + data + ")");
                        if($networkCompletionStatuses)
                        {
                            for(var $i=0;$i<$networkCompletionStatuses.length;$i++)
                            {
                                $CompletionStatusDropDownList += '<option value="'+$networkCompletionStatuses[$i]['CompletionStatusID']+'" >'+$networkCompletionStatuses[$i]['CompletionStatusName']+'</option>';
                            }
                        }
                        $("#CompletionStatusID").html($CompletionStatusDropDownList);
                    });
                }
            }
        });
        $("#ClientID").combobox({
            change: function() {
                $utDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';           
                var $ClientID = $("#ClientID").val();
                if($ClientID && $ClientID!='')
                {
                    $.post("{$_subdomain}/ProductSetup/pricingStructure/getUnitTypes/"+urlencode($ClientID)+"/", '', function(data){
                        var $clientUnitTypes = eval("(" + data + ")");
                        if($clientUnitTypes)
                        {
                            for(var $i=0;$i<$clientUnitTypes.length;$i++)
                            {
                                $utDropDownList += '<option value="'+$clientUnitTypes[$i]['UnitTypeID']+'" >'+$clientUnitTypes[$i]['UnitTypeName']+'</option>';
                            }
                        }
                        $("#UnitTypeID").html($utDropDownList);
                    });
                }
            }
        });
        $("#UnitTypeID").combobox();
        $("#ManufacturerID").combobox();
        $("#JobSite").combobox();
        $("#CompletionStatusID").combobox();
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                
                
         });       
        
    </script>
    
    
    <div id="PricingStructureFormPanel" class="SystemAdminFormPanel" >
    
                <form id="PricingStructureForm" name="PricingStructureForm" method="post" action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            
                         <p>
                               <label ></label>
                               <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                         </p>
                       
                         
                         {if $SupderAdmin eq true} 
                         
                          
                          <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="NetworkID" id="NetworkID" class="text" >
                                <option value="" {if $datarow.NetworkID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $datarow.NetworkID eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                          
                          
                          
                          <p>
                            <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="ClientID" id="ClientID" class="text" >
                                <option value="" {if $datarow.ClientID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $clients as $client}

                                    <option value="{$client.ClientID}" {if $datarow.ClientID eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                          
                          
                        {else if $NetworkUser eq true}
                            
                           <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:</label>
                             &nbsp;&nbsp;  
                            <label class="labelHeight"  >  
                             {$datarow.NetworkName|escape:'html'}
                             <input type="hidden" name="NetworkID" value="{$datarow.NetworkID|escape:'html'}" >
                            </label> 
                          </p>
                          
                          <p>
                            <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="ClientID" id="ClientID" class="text" >
                                <option value="" {if $datarow.ClientID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $clients as $client}

                                    <option value="{$client.ClientID}" {if $datarow.ClientID eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                          
                        {else}    
                          
                                <p>
                                    <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:</label>
                                    &nbsp;&nbsp;  
                                    <label class="labelHeight"  >  
                                    {$datarow.NetworkName|escape:'html'}
                                    <input type="hidden" name="NetworkID" value="{$datarow.NetworkID|escape:'html'}" >
                                    </label> 
                                </p>
                          
                                <p>
                                    <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:<sup>*</sup></label>

                                    &nbsp;&nbsp;  
                                     <label class="labelHeight"  >  
                                    {$datarow.ClientName|escape:'html'}
                                    <input type="hidden" name="ClientID" value="{$datarow.ClientID|escape:'html'}" >
                                    </label> 
                               </p>
                        {/if}    
                         
                        
                          <p>
                            <label class="fieldLabel" for="UnitTypeID" >{$page['Labels']['unit_type']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="UnitTypeID" id="UnitTypeID" class="text" >
                                <option value="" {if $datarow.UnitTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $unitTypes as $unitType}

                                    <option value="{$unitType.UnitTypeID}" {if $datarow.UnitTypeID eq $unitType.UnitTypeID}selected="selected"{/if}>{$unitType.UnitTypeName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                         </p>
                        
                        
                          <p>
                            <label class="fieldLabel" for="ManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="ManufacturerID" id="ManufacturerID" class="text" >
                                <option value="" {if $datarow.ManufacturerID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $manufacturers as $manufacturer}

                                    <option value="{$manufacturer.ManufacturerID}" {if $datarow.ManufacturerID eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                        
                        
                         <p>
                            <label class="fieldLabel" for="JobSite" >{$page['Labels']['job_site']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="JobSite" id="JobSite" class="text" >
                                <option value="" {if $datarow.JobSite eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $jobSites as $jobSite}

                                    <option value="{$jobSite.JobSiteCode}" {if $datarow.JobSite eq $jobSite.JobSiteCode}selected="selected"{/if}>{$jobSite.JobSiteName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                          
                          
                          
                         <p>
                            <label class="fieldLabel" for="CompletionStatusID" >{$page['Labels']['completion_status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="CompletionStatusID" id="CompletionStatusID" class="text" >
                                 
                                <option value="" {if $datarow.CompletionStatusID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $completionStatuses as $completionStatus}

                                    <option value="{$completionStatus.CompletionStatusID}" {if $datarow.CompletionStatusID eq $completionStatus.CompletionStatusID}selected="selected"{/if}>{$completionStatus.CompletionStatusName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                          
                         
                        
                         <p>
                            
                             <label class="fieldLabel" for="ServiceRate" >{$page['Labels']['service_rate']|escape:'html'}:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="ServiceRate" value="{$datarow.ServiceRate|escape:'html'}" id="ServiceRate" >
                        
                         </p>
                         
                         
                         
                          <p>
                            
                             <label class="fieldLabel" for="ReferralFee" >{$page['Labels']['referral_fee']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="ReferralFee" value="{$datarow.ReferralFee|escape:'html'}" id="ReferralFee" >
                        
                         </p>
                       
                          
                         
                         
                          
                             
                        <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>

                            &nbsp;&nbsp; 

                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    
                        </p>
                            
                         

                        <p>

                            <span class= "bottomButtons" >

                                <input type="hidden" name="UnitPricingStructureID"  value="{$datarow.UnitPricingStructureID|escape:'html'}" >


                                {if $datarow.UnitPricingStructureID neq '' && $datarow.UnitPricingStructureID neq '0'}

                                        <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                                {else}

                                    <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                                {/if}

                                    <br>
                                    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                            </span>

                        </p>

                           
                </fieldset>    
                        
                </form>        
                                             
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
