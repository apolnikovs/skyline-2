<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Requisition</title>

</head>

<body>
    {$totSuppliers=$data|@count}
    {$supCounter=0}
    {foreach from=$data key=req item=item }
    {$supCounter=$supCounter+1}  
<style>
.telegistics{
	/*font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif;*/
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
}
.orange{
	color:#{$item['SP']['DocColour1']};
	margin:0px;
	font-weight:bold;
	text-transform:uppercase;
}
@media all {
  .page-break  { display: none; }
}

@media print {
  .page-break  { display: block; page-break-before: always; }
}
</style>
<table class="telegistics page-break" width="610" height="100" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="300" rowspan="5"><img src="{if $item['SP']['DocLogo']!=""}{$_subdomain}/images/serviceProvidersLogos/{$item['SP']['DocLogo']}{else}{$_subdomain}/images/skylineDocLogo.png{/if}"  alt="logo" /></td>
    <td width="260"><h1 class="orange">Goods Received Note</h1></td>
  </tr>
  <tr>
    <td>GRN Number: <span style="font-weight:bold;">{$req}</span></td>
  </tr>
  <tr>
    <td>Date Printed: <span style="font-weight:bold;">{$smarty.now|date_format:"%d/%m/%Y"}</span></td>
  </tr>
  <tr>
    <td>Order Number: <span style="font-weight:bold;">{$item['Order']['number']}</span></td>
  </tr>
  <tr>
    <td>Order Date: <span style="font-weight:bold;">{$item['Order']['orderDate']|date_format:"%d/%m/%Y"}</span></td>
  </tr>
</table>
<!--horizontal line -->
<table style="background:#{$item['SP']['DocColour2']}; height:5px;width:700px;" >
<tr></tr>
</table>
<!--addres table -->
<table class="telegistics" width="610" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
  <tr>
      <td style="width:50%"><h3 class="orange">SUPPLIER ADDRESS</h3></td>
    <td style="width:50%"><h3 class="orange">DELIVERY ADDRESS</h3></td>
  </tr>
  <tr>
    <td>{$item['Supplier']['name']}</td>
    <td>{$item['SP']['CompanyName']}</td>
  </tr>
  <tr>
    <td>{$item['Supplier']['BuildingNameNumber']}</td>
    <td>{$item['SP']['BuildingNameNumber']}</td>
  </tr>
  <tr>
    <td>{$item['Supplier']['Street']}</td>
    <td>{$item['SP']['Street']}</td>
  </tr>
  <tr>
    <td>{$item['Supplier']['LocalArea']} {$item['Supplier']['TownCity']}</td>
    <td>{$item['SP']['LocalArea']} {$item['SP']['TownCity']}</td>
  </tr>
  <tr>
    <td>{$item['Supplier']['PostCode']}, {$item['Supplier']['CountryName']}</td>
    <td>{$item['SP']['PostalCode']}, {$item['SP']['CountryName']}</td>
  </tr>
  <tr>
    <td>Tel: {$item['Supplier']['TelephoneNo']}</td>
    <td>Tel: {$item['SP']['ContactPhone']}</td>
  </tr>
  <tr>
    <td>Fax: {$item['Supplier']['FaxNo']}</td>
    <td>Ext: {$item['SP']['ContactPhoneExt']}</td>
  </tr>
</table>
<!--horizontal line -->
<table style="background:#{$item['SP']['DocColour2']}; height:5px;width:700px;margin-top:5px;" >
<tr></tr>
</table>

<!--description table -->
<table class="telegistics" border="0" cellpadding="1" cellspacing="0"  style="width:700px;margin-top:25px;">
<tr style="background:#{$item['SP']['DocColour2']}; height:15px;width:700px;margin-top:25px;#{$item['SP']['DocColour3']}">
<th style="color:#{$item['SP']['DocColour3']};text-align: left;" width="8%">QTY <br> Rcvd</th>
<th style="color:#{$item['SP']['DocColour3']};text-align: left;" width="20%">PART NUMBER</th>
<th style="color:#{$item['SP']['DocColour3']};text-align: left;" width="37%">PART DESCRIPTION</th>
<th style="color:#{$item['SP']['DocColour3']};text-align: left;" width="15%">GRN <br> VALUE {$item['SP']['CurencyCode']}</th>
<th style="color:#{$item['SP']['DocColour3']};text-align: left;" width="20%">GRN <br> VALUE {$item['Supplier']['CurencyCode']}</th>
</tr>
{$lineCounter=0}
{$itemCounter=0}
{$costCounter=0}
{foreach $item['items'] as $st}
    {$lineCounter=$lineCounter+1}
    {$itemCounter=$itemCounter+$st.qty}
    {$costCounter=$costCounter+($st.qty*$st.UnitCost)}
<tr>
<td width="8%">{$st.qty}</td>
<td width="20%">{$st.PartNo}</td>
<td width="37%">{$st.PartDescription}</td>
<td width="15%">{$st.UnitCost}</td>
<td width="20%">{($item['SP']['ExRate']*$st.UnitCost)|string_format:"%.2f"}</td>
</tr>
{/foreach}
</table>
<!-- summary table -->
<table class="telegistics" width="610" border="0" cellspacing="0" cellpadding="5" style="margin-top:25px;">
  <tr>
    <td width="50%"><h3 class="orange">SUMMARY</h3></td>
    <td width="50%"><h3 class="orange">RECEIVED BY</h3></td>
  </tr>
  <tr>
  <td>Total lines: <span style="font-weight:bold;">{$lineCounter}</span></td>
  <td>Name: <span>........................................................................</span></td>
  </tr>
  <tr>
  <td>Total Items: <span style="font-weight:bold;">{$itemCounter}</span></td>
  <td>Position: <span>.....................................................................</span></td>
  </tr>
  <tr>
  <td>Total GRN Value: {$item['SP']['CurencyCode']} <span style="font-weight:bold;">{$costCounter|string_format:"%.2f"} </span></td>
  <td>Signature: <span>..................................................................</span></td>
  </tr>
  <tr>
  <td>Total GRN Value: {$item['Supplier']['CurencyCode']} <span style="font-weight:bold;margin-left:100px;">{($costCounter * $item['SP']['ExRate'])|string_format:"%.2f"} </span></td>
  <td>Date: <span>..........................................................................</span></td>
  </tr>
  </table>
  {if $totSuppliers!=$supCounter}
 <p class="page-break">    </p>
 {/if}
{/foreach}
</body> 