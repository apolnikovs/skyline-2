{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    
<script type="text/javascript" >
 $(document).ready(function() {  
                    $("#jncClientID").attr('disabled', '');
                    $("#jncBranchID").attr('disabled', '');
                    
                    $(document).on('click', '#goto_booing_btn', 
                        function() {
                           
                            $('#JobNetworkClientForm').validate({


                                        rules:  {
                                                    NetworkID:
                                                    {
                                                            required: true

                                                    },
                                                    ClientID:
                                                    {
                                                           required: true
                                                    },
                                                    BranchID:
                                                    {
                                                           required: true
                                                    },
                                                    JobTypeID:
                                                    {
                                                           required: true
                                                    }

                                        },
                                        messages: {
                                                    NetworkID:
                                                    {
                                                            required: "Please select Network."
                                                    },
                                                    ClientID:
                                                    {
                                                            required: "Please select Client."
                                                    },
                                                    BranchID:
                                                    {
                                                            required: "Please select Branch."
                                                    },
                                                    JobTypeID:
                                                    {
                                                            required: "Please select Job Type."
                                                    }
                                        },

                                        errorPlacement: function(error, element) {
                                              
                                                error.insertAfter( element );

                                        },
                                        errorClass: 'fieldError',
                                        onkeyup: false,
                                        onblur: false,
                                        errorElement: 'label'
                                        
                                        
                                        
                                        });




                        });
                
 
 
 
 
                    /* Add a change handler to the network dropdown - strats here*/
                    $(document).on('change', '#jncNetworkID', 
                        function() {

                               $clientDropDownList  = '<option value="">Select from drop down</option>';
                                
                                var $NetworkID = $("#jncNetworkID").val();

                                if($NetworkID && $NetworkID!='')
                                {

                                    //Getting clients for selected network.   
                                    $.post("{$_subdomain}/Data/getClients/"+urlencode($NetworkID),        

                                    '',      
                                    function(data){
                                            var $networkClients = eval("(" + data + ")");

                                            if($networkClients)
                                            {

                                                for(var $i=0;$i<$networkClients.length;$i++)
                                                {

                                                    $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                                                }
                                            }

                                            $("#jncClientID").removeAttr('disabled');
                                            $("#jncClientID").html('');
                                            $("#jncClientID").html($clientDropDownList);

                                    });
                                }    
                                else
                                {
                                    $("#jncClientID").attr('disabled', '');
                                }
                        }      
                    );
                   /* Add a change handler to the network dropdown - ends here*/
                   
                   /* Add a change handler to the client dropdown - strats here*/
                    $(document).on('change', '#jncClientID', 
                        function() {

                               $branchDropDownList  = '<option value="">Select from drop down</option>';
                                
                                var $ClientID = $("#jncClientID").val();

                                if($ClientID && $ClientID!='')
                                {

                                    //Getting clients for selected network.   
                                    $.post("{$_subdomain}/Data/getBranches/"+urlencode($ClientID)+"/"+$("#jncNetworkID").val(),        

                                    '',      
                                    function(data){
                                            var $clientBranches = eval("(" + data + ")");

                                            if($clientBranches)
                                            {
                                                for(var $i=0;$i<$clientBranches.length;$i++)
                                                {

                                                    $branchDropDownList += '<option value="'+$clientBranches[$i]['BranchID']+'" >'+$clientBranches[$i]['BranchName']+'</option>';
                                                }
                                            }

                                            $("#jncBranchID").removeAttr('disabled');
                                            $("#jncBranchID").html('');
                                            $("#jncBranchID").html($branchDropDownList);

                                    });
                                } 
                                else
                                {
                                    $("#jncBranchID").attr('disabled', '');
                                }
                                    
                        }      
                    );
                   /* Add a change handler to the client dropdown - ends here*/
              

            });
     

</script>

<form action="{$_subdomain}/Job/index" id="JobNetworkClientForm" name="JobNetworkClientForm" method="post" >
    <fieldset>
        <legend title="">New Job Booking</legend>
       
        
        {if $showNetworkDropDown}
        
            <p>
                <label for="NetworkID" >Network:<sup>*</sup></label>
                &nbsp;&nbsp;  <select name="NetworkID" id="jncNetworkID" class="text" >

                        <option value="" selected="selected">Select from drop down</option>

                       {foreach $networks as $network}

                            <option value="{$network.NetworkID}" >{$network.CompanyName|escape:'html'}</option>

                        {/foreach}
                       </select>
            </p>
        {else}
            <input type="hidden" name="NetworkID" id="jncNetworkID"  value="{$NetworkID|escape:'html'}" >
        {/if}    
        
        
        
        
        {if $ClientID}
              <input type="hidden" name="ClientID" id="jncClientID"  value="{$ClientID|escape:'html'}" >
        {else}
           <p>
               <label for="ClientID" >Client:<sup>*</sup></label>
               &nbsp;&nbsp;  <select name="ClientID" id="jncClientID" class="text" >

                               <option value="" selected="selected">Select from drop down</option>

                              {foreach $clients as $client}

                                   <option value="{$client.ClientID}" >{$client.ClientName|escape:'html'}</option>

                               {/foreach}
                              </select>
           </p>
        {/if}
        
        

           <p>
               <label for="BranchID" >Branch:<sup>*</sup></label>
               &nbsp;&nbsp;  <select name="BranchID" id="jncBranchID" class="text" >

                               <option value="" selected="selected">Select from drop down</option>

                              {foreach $branches as $branch}

                                   <option value="{$branch.BranchID}" >{$branch.BranchName|escape:'html'}</option>

                               {/foreach}
                              </select>
           </p>

        
        
        {if $JobTypeID}
            <input type="hidden" name="JobTypeID" id="jncJobTypeID"  value="{$JobTypeID|escape:'html'}" >
            
        {else}
            <p>
                <label for="JobTypeID" >Job Type:<sup>*</sup></label>
                &nbsp;&nbsp;  <select name="JobTypeID" id="jncJobTypeID" class="text" >

                        <option value="" selected="selected">Select from drop down</option>

                       {foreach $jobTypes as $jobType}

                            <option value="{$jobType.JobTypeID}" >{$jobType.Name|escape:'html'}</option>

                        {/foreach}
                       </select>
            </p>
        {/if} 
        
        
        
        
        <p>
            <label  >&nbsp;</label>
            
            <input type="submit" name="goto_booing_btn" id="goto_booing_btn" class="btnStandard"  value="Go" >
            
           
        </p>
    </fieldset>
</form>