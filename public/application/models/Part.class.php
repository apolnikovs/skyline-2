<?php
require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Part.class.php
 * 
 * This model handles interaction with the parts table.
 * 
 * @author      Andrew J. Williams <a.williams@pccsuk.com>
 * @version     1.07
 * @copyright   2012 - 2013 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author               Reason
 * 22/06/2012  1.00    Andrew J. Williams   Initial Version
 * 16/08/2012  1.01    Andrew J. Williams   Added getByJobId
 * 20/08/2012  1.02    Andrew J. Williams   Added various rotines to support Samsung API
 * 22/08/2012  1.03    Andrew J. Williams   Added updateAcceptedCost
 * 13/11/2012  1.04    Andrew J. Willliams  Added getNoneOtherAdjustmentByJobId
 * 19/12/2012  1.05    Andrew J. Williams   Issue 163 - Duplicate Part Entry Created
 * 05/02/2012  1.06    Vykintas Rutkunas    Job update parts table
 * 19/04/2013  1.07    Andrew J. Williams   Data Integrity Check - Refresh Process
 ******************************************************************************/

class Part extends CustomModel {
    private $table;                                                             /* For Table Factory Class */
    public $debug = false;
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::Part();
    }
    
    /**
     * create
     *  
     * Create a part
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new part
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        $this->Execute($this->conn, $cmd, $args);
        
        $id = $this->conn->lastInsertId();
        
        if ( $id == 0 ) {                                                       /* No id of new insert so error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Created';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'id' => $id
                     )
               );
    }
    
    /**
     * update
     *  
     * Update a part
     * 
     * @param array $args   Associative array of field values for to update the
     *                      part. Not this must include the primary key (PartID)
     *                      for the record to be updated.
     * 
     * @return array    (status - Status Code, message - Status message, rows_affected number of rows updated)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Updated';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    /**
     * delete
     *  
     * Delete a part from the parts table. We can delete on either PartID the 
     * native SkyLine Part ID or SBPartID the service base Part ID. To achive 
     * this we pass an array as a paremeter. The index of the array should match
     * the field we are deleting on (PartID or SBPartID) and the Value an integer
     * of the ID. The array passed should only have one item.
     * 
     * @param array $args (Field { PartID, SBPartID } => Value )
     * 
     * @return (status - Status Code, message - Status message, rows_affected number of rows deleted)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function delete($args) {
        $index  = array_keys($args); 
        
        $cmd = $this->table->deleteCommand( $index[0].' = '.$args[$index[0]] );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Deleted';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    
    /**
     * deleteByJobId
     *  
     * Delete all the parts for a specific job.
     * 
     * This is specifcally used in Data Integrity Check Refresh Process
     * 
     * @param array $jId    JobID
     * 
     * @return true if sucessful, false otherwise
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function deleteByJobId($jId) {
        $sql = "
                DELETE
		FROM
			`part`
		WHERE
			`JobID` = $jId
               ";
        
        $count = $this->conn->exec($sql);

        if ( $count !== false ) {
            return(true); 
        } else {
            return(false);                         
        }
    }
        
    
    /**
     * getByJobId
     *  
     * Return the details from the parts table for a specific job
     * 
     * @param array $jId    JobID
     * 
     * @return assoicative array with parts details
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getByJobId($jId) {
        $sql = "
                SELECT
			*
		FROM
			`part`
		WHERE
			`JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Parts exist - return all records */
        } else {
            return(null);                                                       /* Not found return null */
        }
        
    }
    
    /**
     * getNoneOtherAdjustmentByJobId
     *  
     * Return the details from the parts table for a specific job provided the
     * parts entry are not Adjustments or other costs
     * 
     * @param array $jId    JobID
     * 
     * @return assoicative array with parts details
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getNoneOtherAdjustmentByJobId($jId) {
        $sql = "
                SELECT
			*
		FROM
			`part`
		WHERE
        		UPPER(`IsOtherCost`) != 'Y'
			AND UPPER(`IsAdjustment`) != 'Y'
			AND `JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Parts exist - return all records */
        } else {
            return(null);                                                       /* Not found return null */
        }
        
    }
    
    /**
     * getIRISCode
     *  
     * Returns the thje IRIS codes for the parts for a given job, This is used 
     * by the Samsung API
     * 
     * @param array $jId    JobID
     * 
     * @return assoicative array with the IRIS details
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getIRISCode($jId) {
        $sql = "
                SELECT
			`DefectCode` AS `IRISDefectCode`,
			`RepairCode` AS `IRISRepairCode`,
			`SectionCode` AS `IRISSection`
		FROM
			`part`
		WHERE
			(
				`IsAdjustment` = 'Y'
				OR `IsMajorFault` != 0
			)
			AND `JobID` = $jId
		GROUP BY 
			`IsAdjustment`
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* IRIS information exisits so return */
        } else {
            return(                                                             /* Not found return blank IRIS coses */
                   array(
                         'IRISDefectCode' => '',
			 'IRISRepairCode' => '',
			 'IRISSection' => ''
                        )
                  );                                                       
        }
    }
    
    /**
     * getTotalUnitCost
     *  
     * Returns the total unit cost for all parts for a certain job excluding the
     * parts that are marked as OtherCost.
     * 
     * @param array $jId    JobID
     * 
     * @return float containing total cost
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getTotalUnitCost($jId) {
        $sql = "
                SELECT
			SUM(`UnitCost`) AS TotalUnitCost
		FROM
			`part`
		WHERE
			NOT `IsOtherCost` <=> 'Y'	
			AND `JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['TotalUnitCost']);                                /* Parts found so return total */
        } else {
            return(0.00);                                                       /* No Parts found so total is nothing */
        }
    }
    
    /**
     * getOtherCost
     *  
     * Returns the other cost and description listed in the parts table for a 
     * specific job.If multiples are listed will return the total for all parts 
     * marked as an "other cost" and all descriptions seprated by commas
     * 
     * @param array $jId    JobID
     * 
     * @return associative array 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getOtherCost($jId) {
        $sql = "
                SELECT
			SUM(`UnitCost`) AS TotalOtherCost,
                        GROUP_CONCAT(`PartDescription`) AS `Description`
		FROM
			`part`
		WHERE
			`IsOtherCost` = 'Y'	
			AND `JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Parts found so return total and concatednated if multiple description(s) */
        } else {
            return(
                   array(                                                       /* No Parts found so total is nothing and no description */
                         'TotalOtherCost' => 0.00,
                         'Description' => ''
                        )
                  );                                                       
        }
    }
    
    /**
     * hasAdditionalFreight
     *  
     * Returns wether a specifc job has parts listed for additional freight.
     * 
     * @param array $jId    JobID
     * 
     * @return boolean containing result 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function hasAdditionalFreight($jId) {
        $sql = "
                SELECT
			`PartID`
                FROM
			`part`
		WHERE
			`PartDescription` = 'Additional Freight'	
			AND `JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return(true);                                                       /* Parts found so return true */
        } else {
            return(false);                                                      /* No Additioonal freight so return false */                                             
        }
    }
    
    /**
     * getSamsungPartsLineList
     *  
     * Returns the parts line list for use by the SamsungAPI
     * 
     * @param array $jId    JobID
     * 
     * @return boolean containing result 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getSamsungPartsLineList($jId) {
        $sql = "
                SELECT
			`PartNo` AS `PartNumber`,
			`CircuitReference` AS `Location`,
			`Quantity` AS `QuantityUsed`,
			`UnitCost` AS `PartsAmount`,
			`InvoiceNo` AS `PartsInvoiceNumber`,
			`PartSerialNo` AS `PartSerialNumber`
                FROM
			`part`
		WHERE
			NOT (
				`PartDescription` = 'Additional Freight'
				OR `IsAdjustment` <=> 'Y'
				OR `IsOtherCost` <=> 'Y'
			)	
			AND `JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Parts found so return true */
        } else {
            return(                                                             /* No matches return null */
                   array(
                        'PartNumber' => '',
			'Location' => '',
			'QuantityUsed' => '',
			'PartsAmount' => '',
			'PartsInvoiceNumber' => '',
			'PartSerialNumber'  => ''
                        )
                  );
        }
    }
    
    /**
     * updateAcceptedCost
     *  
     * Update the parts list with the Samsung accepted cost
     * 
     * @param array $pNo    Part No
     *              $ac     Accepted Cost
     * 
     * @return boolean containing result 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function updateAcceptedCost($pNo, $ac) {
        $sql = "
                UPDATE
			`parts`
		SET
			`UnitCost` = $ac
		WHERE
			`PartNo` = $pNo
               ";
        
        if ($this->Execute($this->conn, $sql)) {
            $result =  array('status' => 'SUCCESS',
                             'message' => '',);
        } else {
            $result = array('status' => 'FAIL',
                            'message' => $this->lastPDOError());          
        }
        return $result;
    }
    
    /**
     * getPartIdFromSbId
     *  
     * Returns the the Part ID based upon Servicebase Part ID and Job ID
     * 
     * @param array  $sId    Service Base Job ID
     *               $jId    JobID
     * 
     * @return assoicative array with the IRIS details
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getPartIdFromSbId($sId, $jId) {
        $sql = "
                SELECT
			`PartID`
		FROM
			`part`
		WHERE
			`SBPartID` = $sId 
                        AND `JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['PartID']);                                       /* Part exisits so return Part ID*/
        } else {
            return(null);                                                       /* Not found return Null */
        }
    }
    
    
    
    /**
    @action get part data (Job Update page)
    @author Vykintas Rutkunas <v.rutkunas@pccsuk.com>
    @input  part ID
    @output void
    @return part data array
    */
    
    public function getPartByID($partID) {
	
	$q = "SELECT * FROM part WHERE PartID = :partID";
	$values = ["partID" => $partID];
	$result = $this->query($this->conn, $q, $values);
	if(isset($result[0])) {
	    $result[0]["OrderDate"] = $result[0]["OrderDate"] ? date("d/m/Y", strtotime($result[0]["OrderDate"])) : null;
	    $result[0]["DueDate"] = $result[0]["DueDate"] ? date("d/m/Y", strtotime($result[0]["DueDate"])) : null;
	    $result[0]["ReceivedDate"] = $result[0]["ReceivedDate"] ? date("d/m/Y", strtotime($result[0]["ReceivedDate"])) : null;
	}
	return (isset($result[0])) ? $result[0] : false;
	
    }
    

    
    /**
    @action create/update part (Job Update page)
    @author Vykintas Rutkunas <v.rutkunas@pccsuk.com>
    @input  part data
    @output void
    @return void
    */
    
    function updatePart($data) {
	
        
        //check if major fault then first set all parts major falts to 0 so only one part is major faoult
        if(isset($data["IsMajorFault"])){
            $j=$data["JobID"];
            if($j!=""){
        $sql="update part set IsMajorFault=0 where JobID=$j";
         $result = $this->Execute($this->conn, $sql);
            }
        }
	$data["OrderDate"] = str_replace("/", "-", $data["OrderDate"]);
	$data["DueDate"] = str_replace("/", "-", $data["DueDate"]);
	$data["ReceivedDate"] = str_replace("/", "-", $data["ReceivedDate"]);
	
	$values = [
	    "PartDescription" => (isset($data["PartDescription"]) ? $data["PartDescription"] : null),
	    "PartNo" => (isset($data["PartNo"]) ? $data["PartNo"] : null),
	    "Quantity" => (isset($data["Quantity"]) ? $data["Quantity"] : null),
	    "Supplier" => (isset($data["Supplier"]) ? $data["Supplier"] : null),
	    "SupplierID" => (isset($data["SupplierID"]) ? $data["SupplierID"] : null),
	    "OrderNo" => (isset($data["OrderNo"]) ? $data["OrderNo"] : null),
	    "OrderDate" => ((isset($data["OrderDate"]) && $data["OrderDate"] != "") ? date("Y-m-d" ,strtotime($data["OrderDate"])) : null),
	    "DueDate" => ((isset($data["DueDate"]) && $data["DueDate"] != "") ? date("Y-m-d", strtotime($data["DueDate"])) : null),
	    "ReceivedDate" => ((isset($data["ReceivedDate"]) && $data["ReceivedDate"] != "") ? date("Y-m-d", strtotime($data["ReceivedDate"])) : null),
	    "PartStatus" => (isset($data["PartStatus"]) ? $data["PartStatus"] : null),
	    "SectionCodeID" => (isset($data["SectionCodeID"]) ? $data["SectionCodeID"] : null),
	    "DefectCodeID" => (isset($data["DefectCodeID"]) ? $data["DefectCodeID"] : null),
	    "RepairCodeID" => (isset($data["RepairCodeID"]) ? $data["RepairCodeID"] : null),
	    "IsMajorFault" => (isset($data["IsMajorFault"]) ? $data["IsMajorFault"] : 0),
	    "CircuitReference" => (isset($data["CircuitReference"]) ? $data["CircuitReference"] : null),
	    "PartSerialNo" => (isset($data["PartSerialNo"]) ? $data["PartSerialNo"] : null),
	    "PartStatusID" => (isset($data["PartStatusID"]) ? $data["PartStatusID"] : null),
	    "InvoiceNo" => (isset($data["InvoiceNo"]) ? $data["InvoiceNo"] : null),
	    "UnitCost" => (isset($data["UnitCost"]) ? $data["UnitCost"] : null),
	    "SaleCost" => (isset($data["SaleCost"]) ? $data["SaleCost"] : null),
	    "VATRate" => (isset($data["VATRate"]) ? $data["VATRate"] : null),
	    "SBPartID" => (isset($data["SBPartID"]) ? $data["SBPartID"] : null),
	    "IsOtherCost" => (isset($data["IsOtherCost"]) ? $data["IsOtherCost"] : 0),
	    "IsAdjustment" => (isset($data["IsAdjustment"]) ? $data["IsAdjustment"] : 0),
	    "SupplierOrderNo" => (isset($data["SupplierOrderNo"]) ? $data["SupplierOrderNo"] : null),
	    "SpPartStockTemplateID" => (isset($data["SpPartStockTemplateID"]) ? $data["SpPartStockTemplateID"] : null),
	    "ChargeType" => (isset($data["ChargeType"]) ? trim($data["ChargeType"]) : null)
	];
	
	if($data["PartID"]) {

	    $values["PartID"] = $data["PartID"];
	    
	    $q = "  UPDATE  part
		
		    SET	    PartDescription = :PartDescription,
			    PartNo = :PartNo,
			    Quantity = :Quantity,
			    Supplier = :Supplier,
			    SupplierID = :SupplierID,
			    OrderNo = :OrderNo,
			    OrderDate = :OrderDate,
			    DueDate = :DueDate,
			    ReceivedDate = :ReceivedDate,
			    PartStatus = :PartStatus,
			    PartSectionCodeID = :SectionCodeID,
			    PartDefectCodeID = :DefectCodeID,
			    PartRepairCodeID = :RepairCodeID,
			    IsMajorFault = :IsMajorFault,
			    CircuitReference = :CircuitReference,
			    PartSerialNo = :PartSerialNo,
			    OrderNo = :OrderNo,
			    PartStatusID = :PartStatusID,
			    InvoiceNo = :InvoiceNo,
			    UnitCost = :UnitCost,
			    SaleCost = :SaleCost,
			    VATRate = :VATRate,
			    SBPartID = :SBPartID,
			    IsOtherCost = :IsOtherCost,
			    IsAdjustment = :IsAdjustment,
			    SupplierOrderNo = :SupplierOrderNo,
			    SpPartStockTemplateID = :SpPartStockTemplateID,
                            ChargeType=:ChargeType

		    WHERE   PartID = :PartID
		 ";
	    
	    $result = $this->Execute($this->conn, $q, $values);
	    
	} else {
	    
	    $values["JobID"] = $data["JobID"];
	    
	    $q = "  INSERT INTO	part
				(
				    JobID,
				    PartDescription,
				    PartNo,
				    Quantity,
				    Supplier,
				    SupplierID,
				    OrderNo,
				    OrderDate,
				    DueDate,
				    ReceivedDate,
				    PartStatus,
				    PartSectionCodeID,
				    PartDefectCodeID,
				    PartRepairCodeID,
				    IsMajorFault,
				    CircuitReference,
				    PartSerialNo,
				    PartStatusID,
				    InvoiceNo,
				    UnitCost,
				    SaleCost,
				    VATRate,
				    SBPartID,
				    IsOtherCost,
				    IsAdjustment,
				    SupplierOrderNo,
                                    SpPartStockTemplateID,
                                    ChargeType
				)
		    VALUES
				(
				    :JobID,
				    :PartDescription,
				    :PartNo,
				    :Quantity,
				    :Supplier,
				    :SupplierID,
				    :OrderNo,
				    :OrderDate,
				    :DueDate,
				    :ReceivedDate,
				    :PartStatus,
				    :SectionCodeID,
				    :DefectCodeID,
				    :RepairCodeID,
				    :IsMajorFault,
				    :CircuitReference,
				    :PartSerialNo,
				    :PartStatusID,
				    :InvoiceNo,
				    :UnitCost,
				    :SaleCost,
				    :VATRate,
				    :SBPartID,
				    :IsOtherCost,
				    :IsAdjustment,
				    :SupplierOrderNo,
                                    :SpPartStockTemplateID,
                                    :ChargeType
				)
		 ";
	    
	    $result = $this->Execute($this->conn, $q, $values);
	    if(!$data["PartID"]) {
                return $this->conn->lastInsertId();
            }
	}
	
    }
    

    
    /**
    @action delete part (Job Update page)
    @author Vykintas Rutkunas <v.rutkunas@pccsuk.com>
    @input  part ID
    @output void
    @return void
    */
    
    function deletePart($partID) {
	
	$q = "	DELETE FROM part 
		WHERE	    CASE
				WHEN @userID := :userID
				THEN PartID = :partID
				ELSE FALSE
			    END
	     ";
	
	$values = ["partID" => $partID, "userID" => $this->controller->user->UserID];
	$result = $this->Execute($this->conn, $q, $values);
	
    }
    
    
    
}

?>
