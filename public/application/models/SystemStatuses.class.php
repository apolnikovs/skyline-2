<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of System Statuses Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class SystemStatuses extends CustomModel {
    
    private $conn;
    private $dbColumns = array('StatusID', 'StatusName', 'Colour', 'Status');
    private $table     = "status";
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function fetch($args) {
        
        
      
           $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);
        
        
            return  $output;
        
     }
     
         
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['StatusID']) || !$args['StatusID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
     
    
    
    
    /**
     * Description
     * 
     * This method is used for to validate status name.
     *
     * @param interger $StatusName  
     * @param interger $StatusID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($StatusName, $StatusID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT StatusID FROM '.$this->table.' WHERE StatusName=:StatusName AND StatusID!=:StatusID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':StatusName' => $StatusName, ':StatusID' => $StatusID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['StatusID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO '.$this->table.' (StatusName, Colour, Status, CreatedDate, ModifiedUserID, ModifiedDate, TimelineStatus, StatusTAT, StatusTATType)
            VALUES(:StatusName, :Colour, :Status, :CreatedDate, :ModifiedUserID, :ModifiedDate, :TimelineStatus, :StatusTAT, :StatusTATType)';
        
        
        if($this->isValidAction($args['StatusName'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(array(':StatusName' => $args['StatusName'], ':Colour' => $args['Colour'], ':Status' => $args['Status'], 
                
                ':CreatedDate' => date("Y-m-d H:i:s"),
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s"),
                ':TimelineStatus' => isset($args['TimelineStatus'])?$args['TimelineStatus']:NULL,
                ':StatusTAT' => isset($args['StatusTAT'])?$args['StatusTAT']:NULL,
                ':StatusTATType' => isset($args['StatusTATType'])?$args['StatusTATType']:NULL
                
                ));
        
        
              return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_inserted_msg']);
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT StatusID, StatusName, Colour, Status, TimelineStatus, StatusTAT, StatusTATType FROM '.$this->table.' WHERE StatusID=:StatusID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':StatusID' => $args['StatusID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
      /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['StatusName'], $args['StatusID']))
        {        
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($this->controller->statuses[1]['Code']==$args['Status'])
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
            }
            
               /* Execute a prepared statement by passing an array of values */
              $sql = 'UPDATE '.$this->table.' SET 
                
              StatusName=:StatusName, Colour=:Colour, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate,
              TimelineStatus=:TimelineStatus, StatusTAT=:StatusTAT, StatusTATType=:StatusTATType

              WHERE StatusID=:StatusID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(
                      
                      array(
                        
                        ':StatusName' => $args['StatusName'], 
                        ':Colour' => $args['Colour'],   
                        ':Status' => $args['Status'],
                        ':EndDate' => $EndDate,
                        ':ModifiedUserID' => $this->controller->user->UserID,
                        ':ModifiedDate' => date("Y-m-d H:i:s"),
                        ':StatusID' => $args['StatusID'],
                        ':TimelineStatus' => isset($args['TimelineStatus'])?$args['TimelineStatus']:NULL,
                        ':StatusTAT' => isset($args['StatusTAT'])?$args['StatusTAT']:NULL,
                        ':StatusTATType' => isset($args['StatusTATType'])?$args['StatusTATType']:NULL  
                
                )
                      
             );
        
                
               return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to preferences status
    *
    * @param string $Page
    * @param string $Type
    * @return array It contains status ids.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function getUserStatusPreferences($Page, $Type) {
        
	/*Execute a prepared statement by passing an array of values*/
        $sql = 'SELECT	StatusID 
		FROM	status_preference 
		WHERE	Page = :Page AND 
			Type = :Type AND 
			UserID = :UserID
	       ';
        
	$selectQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
	$selectQuery->execute([
				':Page' => $Page, 
				':Type' => $Type,   
				':UserID' => $this->controller->user->UserID
			      ]
        );
              
	$result = $selectQuery->fetchAll();
        
	return $result;
        
    }
    
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to update user preferences status (RA Status or Job statuses)
     *
     * @param string $Page
     * @param string $Type
     * @param array  $StatusID
     *  
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function updateUserStatusPreferences($Page, $Type, $StatusID) {
        
        if($Page && $Type)
        {        
            
             //First deleting user preferences of page $Page and type $Type
            
             /* Execute a prepared statement by passing an array of values */
              $sql1 = 'DELETE FROM status_preference WHERE `Page`=:Page AND `Type`=:Type AND UserID=:UserID';
        
              $deleteQuery = $this->conn->prepare($sql1, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $deleteQuery->execute(
                      
                      array(
                        
                        ':Page' => $Page, 
                        ':Type' => $Type,   
                        ':UserID' => $this->controller->user->UserID
                
                )
                      
             );
            
            
            
              /* Execute a prepared statement by passing an array of values */
              $sql2 = 'INSERT INTO status_preference (UserID, `Page`, `Type`, StatusID) VALUES (:UserID, :Page, :Type, :StatusID)';
        
              $insertQuery = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              
              foreach($StatusID AS $sID)
              {
                    $insertQuery->execute(

                            array(

                              ':UserID' => $this->controller->user->UserID, 
                              ':Page' => $Page,   
                              ':Type' => $Type,
                              ':StatusID' => $sID

                      )
                     );
              }  
                
               return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['Text']['data_deleted_msg']);
    }
    
    
    
    public function getEntityStatusList($user) {
	
	/*
	$q = "	SELECT	* 
		FROM	status_permission
		WHERE	CASE
			    WHERE (SELECT COUNT(*) FROM status_permission WHERE PermissionType = 'user' AND EntityID = :userID) > 0
			    THEN PermissionType = 'user' AND EntityID = :userID
			    
			    WHERE (SELECT COUNT(*) FROM status_permission WHERE PermissionType = 'branch' AND EntityID = :branchID) > 0
			    THEN PermissionType = 'branch' AND EntityID = :branchID
			    
			    WHERE (SELECT COUNT(*) FROM status_permission WHERE PermissionType = 'brand' AND EntityID = :brandID) > 0
			    THEN PermissionType = 'brand' AND EntityID = :brandID
			    
			    WHERE (SELECT COUNT(*) FROM status_permission WHERE PermissionType = 'client' AND EntityID = :clientID) > 0
			    THEN PermissionType = 'client' AND EntityID = :clientID
			    
			    WHERE (SELECT COUNT(*) FROM status_permission WHERE PermissionType = 'network' AND EntityID = :networkID) > 0
			    THEN PermissionType = 'network' AND EntityID = :networkID
			END
	     ";
	*/
	
	$q = "	SELECT	    sp.StatusID,
			    status.StatusName
		FROM	    status_permission AS sp 
		LEFT JOIN   status ON sp.StatusID = status.StatusID
		WHERE	    PermissionType = 'user' AND 
			    EntityID = :userID";
	
	$values = ["userID" => $user->UserID];
	$result = $this->query($this->conn, $q, $values);
	return $result;
	
    }
    
    
    
}
?>