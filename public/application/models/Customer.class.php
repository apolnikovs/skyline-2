<?php
require_once('CustomModel.class.php');
require_once('TableFactory.class.php');


/**
 * Description of Customer
 * 
 * This model handles interacttion with the customers table.
 *
 * @author      Simon Tsang <tsang.kuen@gmail.com>
 * 
 * @copyright   2012 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author               Reason
 * ??/??/2012  1.00    Simon Tsang          Initial Version      
 * 21/06/2012  1.01    Andrew J. Williams   Removed logging of error in updateByID for all fields not included as API does not always pass all fieldss
 *                                          Added Phone and E-mail fields to $fields array so these fields can be updadted
 *                                          Added create function to allow creation of customer
 * 03/08/2012  1.02    Andrew J. Williams   Added getBySamsungReference
 * 22/08/2012  1.03    Andrew J. Williams   Added fetchRow 
 * 19/06/2013  1.04    Brian Etherington    Correct column name ContactTitleID to CustomerTitleID
 ******************************************************************************/

class Customer extends CustomModel{
    private $skyline = null;
    private $conn;    
    private $table = "customer";
    private $tbl;                                                               /* For Table Factory */
    public $CustomerID = null;
    
    static private $fields = array(
        'CustomerTitleID',
        'ContactFirstName',
        'ContactLastName',
        'PostalCode',
        'BuildingNameNumber',
        'Street',
        'LocalArea',
        'TownCity',
        'CountyID',
        'CountryID',
        'ContactHomePhone',
        'ContactWorkPhone',
        'ContactWorkPhoneExt',
        'ContactMobile',
        'ContactEmail',
        'SamsungCustomerRef',
        'CustomerType',
        'CompanyName',
        'ContactAltMobile'
    );

    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

        $this->tbl = TableFactory::Customer();
    }

    /**
     * 
     * 
     * @return array
     *
     * @author Simon Tsang s.tsang@pccsuk.com	
     * @tutorial the skyine must be set 
     * @todo get the rest of the array data from database dependant of Brand or Theme
     ********************************************/
    public function details(){
        if(isset($this->skyline)){
            #can't get brand because line 121 in Skyline Model
            #$brands = $this->skyline->getBrand();

            $data = $this->skyline->getCustomer( $this->CustomerID );
            #$this->controller->log('Customer->details :'.var_export($data, true));

            #todo
            $data = array_merge($data, array(
                'PostcodeLookup'=>1,
                'ShowCountry'=>1,
                'CountryMandatory'=>1

            ));

            return $data;
        }
    }
    
    
    
     /**
     * 
     * @param string $ContactEmail
     * @global $this->table
     * @return int It return  CustomerID if customer found otherwise it returns null.
     *
     * @author Nageswara Kanteti nag.phpdeveloper@gmail.com
     ********************************************/
    public function isExists($ContactEmail){
        
        $sql = 'SELECT CustomerID FROM '.$this->table.' WHERE ContactEmail=:ContactEmail';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ContactEmail' => $ContactEmail));
        $result = $fetchQuery->fetch();
        
       
        if(is_array($result) && $result['CustomerID'])
        {
                return $result['CustomerID'];
        }
        else
        {
                return null;
        }
       
    }
    
    
    

    
    public function setSkyline(Skyline $skyline){
        if(is_object($skyline))
            $this->skyline = $skyline;                
    }    
    
    /**
     * 
     * updateByID fn
     * @param array $args
     * @param interger $CustomerID name to declare
     * @return boolean
     * @example /index/jobupdate/34589120 Edit Customer Details
     * @author s.tsang@pccsuk.com
     ********************************************/
    public function updateByID($args, $CustomerID=null) {
        $this->CustomerID = isset($CustomerID) ? $CustomerID : null;
        
        if( isset($this->CustomerID) ){
            $updates = '';
            $where = "WHERE CustomerID=$CustomerID";

            $cursor_param = array();

            foreach( self::$fields as $field ){

                if(isset($args[$field])){
                    
                    if($field=="PostalCode")
                    {
                        $args[$field] = strtoupper($args[$field]);
                    }
                    
                    $updates .= ( $updates=='' ? '' : ',' ) . " $field = :$field" ;

                    $cursor_param[":$field"] = (isset($args[ $field ]) && $args[ $field ]!='')?$args[ $field ]:NULL;
                }else{
                    #$this->controller->log("missing $field in Customer->updaetByID");

                }

            }

            $sql = 'UPDATE '.$this->table.' SET ' . $updates . ' ' . $where;
            
            //$this->controller->log( $args );
            //$this->controller->log( self::$fields );
            //$this->controller->log( $sql );

            $update = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $update->execute( $cursor_param );

            #$this->Query($this->conn, $sql);
            #$this->Execute($this->conn, $sql)



            return $update;
        }
    }
    
    /**
     * create
     *  
     * Create a customer
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new customer
     * 
     * @return integer  ID of new customer or NULL if not vreated
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        
        $cmd = $this->tbl->insertCommand( $args );
        $this->Execute($this->conn, $cmd, $args);
        
        $id = $this->conn->lastInsertId();
        
        if ( $id == 0 ) {                    /* No id of new insert so error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Created';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'id' => $id
                     )
               );
    }
    
    /**
     * getBySamsungReference
     * 
     * Get the Customer ID of a customer based on the Samsung Customer Reference
     * 
     * @param string $scRef    Samsung Customer Reference
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function getBySamsungReference($scRef) {
        $sql = "
                SELECT 
			`CustomerID`
                FROM
			`customer`
		WHERE
			`SamsungCustomerRef` = '$scRef'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['CustomerID']);                                   /* Requested item exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    /**
     * fetchByRow
     * 
     * get row from the database beased on CustomerID. This will return all the
     * information relating.
     * 
     * @param string $cId    Customer ID
     * 
     * @return assocaite array with all the data for a customer.
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function fetchRow($cId) {
        $sql = "
                SELECT 
			*
                FROM
			`customer`
		WHERE
			`CustomerID` = $cId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Requested customer */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
}

?>