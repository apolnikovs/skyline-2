<?php

/**
 * UnitTypeManufacturerProvider.class.php
 * 
 * Model with routines to access network_service_provider table in the Skyline
 * database
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link       
 * @version    1.01
 * 
 * Changes
 * Date        Version Author                Reason
 * 30/01/2013  1.00    Andrew J. Williams    Initial Version (Required in Trackerbase VMS Log 157)
 * 19/02/2013  1.01    Andrew J. Williams    Issue 223 - Undefined UnitTypeID in CustomSOAPController
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class UnitTypeManufacturer extends CustomModel {
    private $table;                                                             /* For Table Factory Class */
    private $tables = 'unit_type_manufacturer utm LEFT JOIN manufacturer mft ON utm.ManufacturerID = mft.ManufacturerID
                                                  LEFT JOIN unit_type ut ON ut.UnitTypeID = utm.UnitTypeID';
    private $columns = array(
                             'UnitTypeManufacturerID', 
                             'UnitTypeName', 
                             'ManufacturerName', 
                             'ProductGroup', 
                             'utm.Status'
                            );
    private $conn;
    public $debug = false;
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
       
        $this->table = TableFactory::UnitTypeManufacturer();
    }
 
    /**
     * fetch
     *  
     * Get Unit Type Manufacturer Records
     * 
     * @param array $args
     * 
     * @return array 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function fetch($args) {
        $mId = isset($args['firstArg']) ? $args['firstArg'] : 0;
        
        if ($mId != 0) {	    
	    $sql = "
                    SELECT
				`UnitTypeManufacturerID` AS `0`,
				`UnitTypeName` AS `1`,
				`ManufacturerName` AS `2`,
				`ProductGroup` AS `3`,
				CASE
				    WHEN utm.`Status`
				    THEN utm.`Status`
				    ELSE 'In-active'
				END AS `4`
		    FROM
				`unit_type_manufacturer` utm LEFT JOIN `manufacturer` mft ON utm.`ManufacturerID` = mft.`ManufacturerID`
                                                             LEFT JOIN `unit_type` ut ON ut.`UnitTypeID` = utm.`UnitTypeID`
                    WHERE
                                utm.`ManufacturerID` = $mId
	    ";
	
	    
	    if(isset($_POST["sSearch"]) && $_POST["sSearch"] != "") {
		$s = $_POST["sSearch"];
		$sql .=	" AND (	ut.UnitTypeName LIKE '%$s%' ||
				mft.ManufacturerName LIKE '%$s%' ||
				utm.Status LIKE '%$s%'
			      )
			";
	    }
	    
	    
	    $values = ["ManufacturerID" => $mId];
	    
	    $result = $this->query($this->conn, $sql, $values);
	    $total = count($result);
	    
            if( isset($_POST["iDisplayStart"]) && isset($_POST["iDisplayLength"]) ) {
                $sql .= " LIMIT " . $_POST["iDisplayStart"] . ", " . $_POST["iDisplayLength"];
            }
	    
	    $result = $this->query($this->conn, $sql, $values);
	    
	    $output = (object)[ "sEcho" => (int)$_POST["sEcho"], 
				"iTotalRecords" => $total, 
				"iTotalDisplayRecords" => $total,
				"aaData" => $result
			      ];
	    
        } else {
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->columns, $args);
        }
        
        return($output);
    }
    
    /**
     * create
     *  
     * Create a Unit Type Manufacturer Record
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new precord
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $args['CreatedDate'] = 'NOW()';
        
        $cmd = $this->table->insertCommand( $args );
        $this->Execute($this->conn, $cmd, $args);
        
        $id = $this->conn->lastInsertId();
        
        if ( $id == 0 ) {                                                       /* No id of new insert so error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Created';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'id' => $id
                     )
               );
    }
    
    /**
     * update
     *  
     * Update a Unit Type Manufacturer Record
     * 
     * @param array $args   Associative array of field values for to update the
     *                      record.
     * 
     * @return array    (status - Status Code, message - Status message, rows_affected number of rows updated)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Updated';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    /**
     * delete
     *  
     * Delete a record from the Unit Type Manufacturer table. 
     * 
     * @param array $args (Field { PartID, SBPartID } => Value )
     * 
     * @return (status - Status Code, message - Status message, rows_affected number of rows deleted)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function delete($args) {
        $index  = array_keys($args); 
        
        $cmd = $this->table->deleteCommand( $index[0].' = '.$args[$index[0]] );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Deleted';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
     *
     * @return array Status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     **************************************************************************/   
     public function processData($args) {
         /* Check if create or update */
         if(!isset($args['UnitTypeManufacturerID']) || !$args['UnitTypeManufacturerID'] || $args['UnitTypeManufacturerID'] == '0')
         {                                                                      /* Create */
             if (isset($args['UnitTypeManufacturerID'])) {                      /* Check if we do have an UnitTypeManufacturerID set to 0 */
                 unset($args['UnitTypeManufacturerID']);                        /* We do so unset it, otherwise will cause a 'SQLSTATE[HY093]: Invalid parameter number error'*/
             } /* fi isset $args['UnitTypeManufacturerID'] */
             return $this->create($args);
         }
         else
         {                                                                      /* Update */
             return $this->update($args);
         } /* fi create or update */
     }
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param  array $args
     * @global $this->table  
     * @global $this->table_unit_client_type
     * 
     * @return array Row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     **************************************************************************/ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT UnitTypeManufacturerID, ManufacturerID, UnitTypeID, ProductGroup, Status FROM unit_type_manufacturer WHERE UnitTypeManufacturerID=:UnitTypeManufacturerID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        $fetchQuery->execute(array(':UnitTypeManufacturerID' => $args['UnitTypeManufacturerID']));
        $result = $fetchQuery->fetch();
        
        return $result;
     }
    
    /**
     * getUnitTypeIdByProductGroup
     *  
     * Return the details from the parts table for a specific job
     * 
     * @param array $pg    Prduct Group
     * 
     * @return integer  The UnitTypeID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getUnitTypeIdByProductGroup($pg) {
        $sql = "
                SELECT
			`UnitTypeID`
		FROM
			`unit_type_manufacturer`
		WHERE
			`ProductGroup` = '$pg'
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['UnitTypeID']);                                   /* Manufacturer product group exists - return all records */
        } else {
            return(null);                                                       /* Not found return null */
        }
        
    }

}

?>
